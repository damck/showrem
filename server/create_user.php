<?php

require_once __DIR__ . '/db/db_connection.php';
require_once __DIR__ . '/phpPasswordHashingLib-master/passwordLib.php';
require_once __DIR__ . '/daos/user_dao.php';
require_once __DIR__ . '/daos/token_dao.php';
require_once __DIR__ . '/models/user.php';
require_once __DIR__ . '/models/token.php';


$response = [];//json
//error = 1 - required fields are missing
//error = 2 - 2 - sql error (duplicated primary key)

if(isset($_POST['username']) && isset($_POST['password'])){
    
    $username = $_POST['username'];
    $password = $_POST['password'];
    if(strlen($username) < 5 || strlen($password) < 5){
        $response["success"] = 0;
        $response["message"] = "Length of the field(s) is too short";
    }else{
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $db = DB_CONNECTION::getConnection();
        $date = date("m/d/y"); 
        // $token = \bin2hex(openssl_random_pseudo_bytes(64));//generating token
	$token = "5bdc571131eee492940c0db69085e5d49ceab75438a08554a7046bd9230eb942bbf5b5f9cda0a7046269fec54ec8d1e8bc45456eb2fd02bb9c32fac2d872cd13";
        $user = new User($username, $hash, "");
        $result = UserDao::save($user);
        $tokenObject = new Token($username, $token);
        $result2 = TokenDao::save($tokenObject);
        if($result && $result2){
            $response['success'] = 1;
            $response['message'] = "User has been created";          
            $response['token'] = $token;
        }else{
            $response['success'] = 0;
            $response['error'] = 2;
            $response['message'] = "SQL error occured: " . mysql_errno() . " " . mysql_error();                        
        }
    }
        
}else{//required fields are missing
    $response["success"] = 0;
    $response["error"] = 1;
    $response["message"] = "Required field(s) is missing";
}
echo json_encode($response);
