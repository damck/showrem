<?php
require_once __DIR__ . '/db_connect.php';

class DB_CONNECTION {

    private static $_instance;

    static function getConnection() {
        if(!isset($_instance)){
            self::$_instance = new DB_CONNECT();
        }
        return self::$_instance;
    }

    static function close() {
        mysql_close();
    }

}

?>