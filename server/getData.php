<?php

require_once __DIR__ . '/db/db_connection.php';
require_once __DIR__ . '/phpPasswordHashingLib-master/passwordLib.php';
require_once __DIR__ . '/daos/token_dao.php';
require_once __DIR__ . '/daos/user_dao.php';

$response = [];//json
//error = 1 - wrong token (cannot authenticate)
//error = 0 - required field(s) is missing
//error = 2 - database error
if(isset($_POST['username']) && isset($_POST['token'])){
    
    $username = $_POST['username'];
    $token = $_POST['token'];


    $db = DB_CONNECTION::getConnection();
    $auth = TokenDao::getTokenForUser($username);
    if($auth){
        $authRow = mysql_fetch_row($auth);
        $tokenFromDatabase = $authRow[0];  
        if(strcmp($token, $tokenFromDatabase) == 0){//authenticate process ended sucessfully
           $temp = UserDao::getUser($username);
           $userQuery = mysql_fetch_assoc($temp);
		   $response['message'] = stripslashes($userQuery['data']);
		   $response['success'] = 1;
		   $response['error'] = 0;
        }else{
            $response['success'] = 0;
            $response['error'] = 1;
            $response['message'] = "Wrong token (cannot authenticate)";
        }
    }else{
        $response['success'] = 0;
        $response['error'] = 1;
        $response['message'] = "Wrong token (cannot authenticate)";                     
    }
    
       
}else{//required fields are missing
    $response["success"] = 0;
    $response["error"] = 0;
    $response["message"] = "Required field(s) is missing";
}
echo json_encode($response);
