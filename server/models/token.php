<?php

class Token{

    private $username;
    private $token;

    function __construct($username, $token){
        $this->username = $username;
        $this->token = $token;
    }

    function getUsername() {
        return $this->username;
    }

    function getToken() {
        return $this->token;
    }

}

?>
