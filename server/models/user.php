<?php

class User{

    private $username;
    private $password;
    private $data;

    function __construct($username, $password, $data){
        $this->username = $username;
        $this->password = $password;
        $this->data = $data;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }
    
    function getData() {
        return $this->data;
    }
    

}

?>
