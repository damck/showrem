<?php

require_once __DIR__ . '/db/db_connection.php';
require_once __DIR__ . '/phpPasswordHashingLib-master/passwordLib.php';
require_once __DIR__ . '/daos/user_dao.php';
require_once __DIR__ . '/daos/token_dao.php';
require_once __DIR__ . '/models/token.php';
require_once __DIR__ . '/models/user.php';

$response = [];//json
//error = 1 - required fields are missing


        
if(isset($_POST['username']) && isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    if(strlen($username) < 5 || strlen($password) < 5){
        $response["success"] = 0;
        $response["message"] = "Length of the field(s) is too short";
    }else{
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $db = DB_CONNECTION::getConnection();
        $date = date("m/d/y"); 

        
        
        $result = UserDao::getPasswordForUser($username);
        if($result){
                $row = mysql_fetch_row($result);
                $hash = $row[0];
                if(password_verify($password, $hash)){//logged successfully
                    $response['success'] = 1;
                    $response['message'] = "You are now logged in";   
                    $token = \bin2hex(openssl_random_pseudo_bytes(64));//generating token
                    TokenDao::deleteTokenForUser($username);
                    $tokenObject = new Token($username, $token);
                    TokenDao::save($tokenObject);
                    $response['token'] = $token;
                }else{
                    $response['success'] = 0;
                    $response['error'] = 1;
                    $response['message'] = "Username and password does not match";
                }
        }else{
            $response['success'] = 0;
            $response['error'] = 0;
            $response['message'] = "SQL error occured: " . mysql_errno() . " " . mysql_error();                        
        }
    }
        
}else{//required fields are missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
}
echo json_encode($response);

