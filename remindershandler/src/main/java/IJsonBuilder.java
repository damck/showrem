import java.util.List;

/**
 * Created by damian on 5/23/17.
 */
public interface IJsonBuilder {
    void buildJson(List<Reminder> p_reminders);

    void tryFixAirDate(String p_type, int p_id, String p_date);

}
