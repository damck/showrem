import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Created by goworko on 5/16/2017.
 */
public class Reminder{
    @JsonProperty("date")
    protected String m_date;

    @JsonProperty("type")
    protected String m_type;

    @JsonProperty("id")
    protected int m_id;

    public Reminder(@JsonProperty("type") String p_type,
                    @JsonProperty("id") int p_id,
                    @JsonProperty("date") String p_date)
    {
        this.m_type = p_type;
        this.m_id = p_id;
        this.m_date = p_date;
    }

    public String getType() { return m_type; }
    public int getId() { return m_id; }
    public String getDate() { return m_date; }

    public Integer getDateDifference(){
        if(m_date != null) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate reminderDate = LocalDate.parse(m_date, format);
            return (int) DAYS.between(LocalDate.now(), reminderDate);
        } else {
            m_date = null;
            return -1;
        }
    }

}
