import java.util.ArrayList;
import java.util.List;

/**
 * Main class for adding/removing and storing Reminders. Eventual ListViews should pull the reminders from here
 * and the JsonBuilder should aim to store what it read from file here.
 */
public class RemindersHandler implements IRemindersHandler{

    private List<Reminder> m_remindersList;

    /**
     * Default constructor, to be used for first init?
     */
    RemindersHandler()
    {
        m_remindersList = new ArrayList<Reminder>();
    }

    /**
     * Constructor that takes a list of Reminder objects, preferably parsed from file by JsonBuilder
     * Might be the only one in the end, with null passed when no file was present.
     * @param p_list
     */
    RemindersHandler(List<Reminder> p_list)
    {
        this.m_remindersList = p_list;
    }

    /**
     * Simply return the list of Reminder objects.
     * @return
     */
    @Override
    public List<Reminder> getRemindersList() {
        return m_remindersList;
    }

    /**
     * Add a Reminder object to list
     * Reminder object could be created by DatabaseWrapper or have its contents pulled
     * as getters are public, to be decided
     * @param p_type
     * @param p_id
     * @param p_airDate
     */
    public void addReminderToList(String p_type, int p_id, String p_airDate) {
        Reminder l_seriesToAdd = new Reminder(p_type, p_id, p_airDate);
        m_remindersList.add(l_seriesToAdd);
    }

    /**
     * Remove a Reminder object from list
     * Needs both type and id as ids aren't intertype-unique
     * @param p_type
     * @param p_id
     */
    public void removeReminderFromList(String p_type, int p_id) {
        for (Reminder l_reminder : m_remindersList)
        {
            if(l_reminder.getType().equals(p_type) && l_reminder.getId() == p_id)
            {
                m_remindersList.remove(l_reminder);
                break;
            }
        }
    }

}
