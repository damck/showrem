import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvEpisode;
import info.movito.themoviedbapi.model.tv.TvSeason;
import info.movito.themoviedbapi.model.tv.TvSeries;
import info.movito.themoviedbapi.tools.WebBrowser;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by goworko on 5/16/2017.
 */
public class DatabaseWrapper implements IDatabaseWrapper{
    TmdbApi m_tvContext;

    /**
     * - sets up the webbrowser as well as the Tmdb context
     */
    DatabaseWrapper()
    {
        WebBrowser browser = new WebBrowser();
        //browser.setProxy("10.158.100.2", "8080", "", "");
        m_tvContext = new TmdbApi("7eaba0fde7defb9f5de4a4d136b39889", browser, true);
    }

    /**
     * @param p_seriesName
     * Should be input by user, used as a query to the database     *
     * @return
     * Returns a list of Show-related Reminder objects prepared from query's results
     */
    @Override
    public List<Reminder> searchForSeries(String p_seriesName) {
        List<Reminder> l_tempList = new ArrayList<>();
        for (TvSeries l_series : m_tvContext.getSearch().searchTv(p_seriesName,"en",1).getResults())
        {
            l_tempList.add(new Reminder("series", l_series.getId(), getSeriesAirdate(l_series.getId())));
        }
        return l_tempList;
    }

    /**
     * Unsure of use right now, individual show pulling could be done by searchForX
     * @param p_id
     * @return
     */
    @Override
    public TvSeries getSeries(int p_id) {
        return m_tvContext.getTvSeries().getSeries(p_id,"en");
    }

    /**
     * @param p_movieName
     * Should be input by user, used as a query to the database
     * @return
     * Returns a list of Movie-related Reminder objects prepared from query's results
     */
    @Override
    public List<Reminder> searchForMovies(String p_movieName) {
        List<Reminder> l_tempList = new ArrayList<>();
        for (MovieDb l_series : m_tvContext.getSearch().searchMovie(p_movieName, null, "en", true, 1).getResults())
        {
            l_tempList.add(new Reminder("series", l_series.getId(), l_series.getReleaseDate()));
        }
        return l_tempList;
    }

    /**
     * Unsure of use right now, individual show pulling could be done by searchForX
     * @param p_id
     * @return
     */
    @Override
    public MovieDb getMovie(int p_id) {
        return m_tvContext.getMovies().getMovie(p_id, "en");
    }

    /**
     * Used only when preparing Show Reminder objects
     * This is due to untrivial access to the very last episode aired
     * @param p_id
     * Show's id, recieved during search query
     * @return
     * Returns a date in String form or a null value if the newest episode has long since aired
     */
    private String getSeriesAirdate(int p_id) {
        TvSeries temp = getSeries(p_id);
        TvSeason tempSeason = m_tvContext.getTvSeasons().getSeason(p_id, temp.getNumberOfSeasons(),"en");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for(TvEpisode ep : tempSeason.getEpisodes()){
            if(LocalDate.parse(ep.getAirDate(), format).compareTo(LocalDate.now()) > 0){
                return ep.getAirDate();
            }
        }
        return null;
    }
}
