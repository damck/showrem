import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.tv.TvSeries;

import java.util.List;

/**
 * Created by goworko on 5/16/2017.
 */
public interface IDatabaseWrapper {
    List<Reminder> searchForSeries(String p_seriesName);

    TvSeries getSeries(int p_id);

    List<Reminder> searchForMovies(String p_seriesName);

    MovieDb getMovie(int p_id);
}
