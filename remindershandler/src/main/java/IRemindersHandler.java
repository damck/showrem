import java.util.List;

/**
 * Created by goworko on 5/16/2017.
 */
public interface IRemindersHandler {
    public List<Reminder> getRemindersList();
    public void addReminderToList(String p_type, int p_id, String p_airDate);
    public void removeReminderFromList(String p_type, int p_id);
}
