import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//TODO rebuildJSON? readAndReturnFromJSON?


public class JsonBuilder implements IJsonBuilder {
    private final String m_pathTo = "";

    private final String m_fileName = "data.json";

    private ObjectMapper m_mapper = new ObjectMapper();


    /**
     * Build a Json file from a current list of Reminder objects
     * stored in the app.
     * @param p_reminders
     */
    @Override
    public void buildJson(List<Reminder> p_reminders) {
        try
        {
            File dataFile = new File(m_pathTo + m_fileName);
            if(!dataFile.exists()){
                dataFile.createNewFile();
            }

            if(FileUtils.fileRead(dataFile).isEmpty())
                FileUtils.fileWrite(m_pathTo + m_fileName, "{}");

            JsonNode root = m_mapper.readTree(new File(m_pathTo + m_fileName));

            if(root.path("reminders").isMissingNode()){
                ArrayNode remindersArray = m_mapper.createArrayNode();
                ((ObjectNode) root).set("reminders", remindersArray);
            }

            if(root.path("userInfo").isMissingNode()){
                ObjectNode userInfo = m_mapper.createObjectNode();
                ((ObjectNode) root).set("dateReminders", userInfo);
            }

            JsonNode remindersNode = root.path("reminders");

            ObjectNode l_tempObj = m_mapper.createObjectNode();

            for (Reminder l_reminder : p_reminders)
            {
                l_tempObj.put("date", l_reminder.getDate());
                l_tempObj.put("id", l_reminder.getId());
                l_tempObj.put("type", l_reminder.getType());
                ((ArrayNode) remindersNode).add(l_tempObj);
            }
            m_mapper.writerWithDefaultPrettyPrinter().writeValue(new File(m_pathTo + m_fileName), root);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads reminders from file
     * If file doesn't exist it creates one and makes it empty.
     * @return
     */
    public List<Reminder> returnRemindersFromFile()
    {
        JsonNode root = null;

        try {
            File dataFile = new File(m_pathTo + m_fileName);
            if(!dataFile.exists()){
                dataFile.createNewFile();
            }

            if(FileUtils.fileRead(dataFile).isEmpty())
                FileUtils.fileWrite(m_pathTo + m_fileName, "{}");

            root = m_mapper.readTree(dataFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Reminder> l_remindersList = new ArrayList<>();
        try {
            l_remindersList = m_mapper.convertValue(root.path("reminders"),
                    new TypeReference<List<Reminder>>() {
                    });
        } catch(NullPointerException e){
            e.printStackTrace();
        }
        return l_remindersList;
    }
    /**
     * Plan is to use this method to update file's individual ReminderNodes on "just aired" basis
     * Needs discussing
     * @param p_type
     * @param p_id
     * @param p_newDate
     */
    @Override
    public void tryFixAirDate(String p_type, int p_id, String p_newDate){
        try
        {
            JsonNode root = m_mapper.readTree(new File(m_pathTo + m_fileName));
            JsonNode remindersNode = root.path("reminders");

            if(remindersNode.isMissingNode()){
                throw new IOException();
            }

            for(JsonNode reminder : remindersNode)
            {
                if(reminder.get("id").intValue() == p_id && reminder.get("type").toString().equals(p_type)){
                    ((ObjectNode) reminder).put("date", p_newDate);
                    break;
                }
            }

            m_mapper.writerWithDefaultPrettyPrinter().writeValue(new File(m_pathTo + m_fileName), root);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
