import info.movito.themoviedbapi.model.tv.TvSeries;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
public class DatabaseWrapperTest {
    private DatabaseWrapper l_testObj = new DatabaseWrapper();

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void searchForSeries() throws Exception {
        Assert.assertThat(l_testObj.searchForSeries("dragon ball super").get(0).getId(), CoreMatchers.is(62715));
    }

    @Test
    public void getSeries() throws Exception {
        TvSeries l_testSeries = l_testObj.getSeries(123);
        Assert.assertThat(l_testSeries.getName(), CoreMatchers.containsString("Starting Over"));
    }

}