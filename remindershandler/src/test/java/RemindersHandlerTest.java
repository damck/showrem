import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by goworko on 5/16/2017.
 */
public class RemindersHandlerTest {

    RemindersHandler l_testObj = new RemindersHandler();

    @Before
    public void setUp() throws Exception {
        l_testObj.addReminderToList("series", 1, "2017-07-01");
    }

    @Test
    public void getRemindersList() throws Exception {
        assertNotNull(l_testObj.getRemindersList());
    }

    @Test
    public void addReminderToList() throws Exception {
        l_testObj.addReminderToList("series", 2, "2017-07-01");
        assertEquals(2, l_testObj.getRemindersList().size());
    }

    @Test
    public void removeSeriesReminderFromList() throws Exception {
        l_testObj.removeReminderFromList("series", 1);
        assertEquals(0, l_testObj.getRemindersList().size());
    }

}